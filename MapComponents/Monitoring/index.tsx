import { useState, useEffect, useRef } from "react";
import { Row, Col, Button, Spinner } from "reactstrap";
import { getOrganizationDD, getProjectListDD, resetStatesSuccess } from "../../../../redux/reducers/dropdowns/dropdown.actions";
import {
  endLoading,
  startLoading,
} from "../../../../redux/reducers/general/general.actions";
import { connect } from "react-redux";
import Select from "react-select";
import { Formik } from "formik";
import { monitoringValidationSchema } from "../../../../constants";
import { AppDispatch } from "../../../../redux/store/store";
import { RootState } from "../../../../redux/reducers/rootReducer";
import { getMapMonitoring, resetMapData } from "../../../../redux/reducers/MapReducer/map.actions";
import { toast } from "react-toastify";

type Props = ReturnType<typeof mapStateToProps> &
  ReturnType<typeof mapDispatchToProps>;

function Monitoring(props: Props) {
  const [selectedFilter, setSelectedFilter] = useState({
    orgId: null,
    projectId: null,
  });
  const formikRef: $TSFixMe = useRef();

  const [isLoading, setIsLoading] = useState(false);

  const apiCall = async () => {
    props.startLoading();
    if (props.orgListDD?.result?.length === 0) {
      props.getOrgListDD();
    }
    props.endLoading();
  };

  const handleFilter: $TSFixMe = async (values: $TSFixMe, type?: string) => {
    try {
      if (type !== "noSubmit") {
        setIsLoading(true);
      }
      let data = {
        organizationManagementId: values.organization.id,
        projectId: values.project.id,
      };
      await props.getMapMonitoring(data);
    } catch (error) {
      console.log(error);
      toast.error(`${(error as $TSFixMe).data.message}`);
    } finally {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    if (selectedFilter.orgId) {
      props.getProjectListDD(selectedFilter.orgId);
    }
    return () => { };
  }, [selectedFilter.orgId]);

  useEffect(() => {
    if (!selectedFilter.orgId && !selectedFilter.projectId) {
      props.resetMapData();
    }
    return () => {
      props.resetMapData();
    };
  }, [selectedFilter]);

  useEffect(() => {
    apiCall();
  }, []);

  useEffect(() => {
    if (props.selectedOrgData) {
      setSelectedFilter((prev: $TSFixMe) => {
        return { ...prev, orgId: props.selectedOrgData.id, projectId: null }
      });
      const data = {
        organization: props.selectedOrgData,
        project: {}
      }
      handleFilter(data, "noSubmit");
      formikRef?.current?.setFieldValue("project", "");
      props.resetStates()
    }
    return () => { }
  }, [props.selectedOrgData.id]);

  return (
    <Row>
      <>
        <Formik
          validationSchema={monitoringValidationSchema}
          ref={formikRef}
          enableReinitialize={true}
          initialValues={{
            organization: props.selectedOrgData ? props.selectedOrgData : {},
            project: {},
          }}
          onSubmit={handleFilter}
        >
          {({ handleBlur, setFieldValue, setTouched, handleSubmit,
            isSubmitting,
            touched,
            errors, }) => (
            <>
              <div
                className={`modulcar mapCard driverScoreCardBody align-items-baseline ${props.theme}`}
              >
                <Col xl={4} md={4} sm={4} className="mapsDropLeft">
                  <Select
                    className={(errors.organization && touched.organization ? "is-invalid " : "")}
                    placeholder="Organization"
                    options={props.orgListDD?.result || []}
                    classNamePrefix={"custom-select-style"}
                    getOptionLabel={(option: $TSFixMe) => option.name}
                    getOptionValue={(option: $TSFixMe) => option.id.toString()}
                    onChange={(option: $TSFixMe) => {
                      setTouched({ ...touched, organization: false });
                      setSelectedFilter((prev) => {
                        return { ...prev, orgId: option.id, projectId: null };
                      });
                      setFieldValue("organization", option);
                      setFieldValue("project", {});
                      props.resetStates();
                    }}
                    value={
                      selectedFilter.orgId
                        ? props.orgListDD?.result?.find(
                          (option: any) => option.id === selectedFilter.orgId
                        )
                        : null
                    }
                    onBlur={handleBlur("organization")}
                    maxMenuHeight={180}
                    menuPortalTarget={document.body}
                    styles={{
                      menuPortal: (base: $TSFixMe) => ({
                        ...base,
                        zIndex: 9999,
                      }),
                    }}
                  />
                  {touched.organization && errors.organization && (
                    <div className="validate">
                      {errors.organization ? "Required" : errors.organization}
                    </div>
                  )}
                </Col>
                <Col xl={4} md={4} sm={4} className="mapsDrop mt-sm-0 mt-2">
                  <Select
                    placeholder="Project"
                    options={props.dropdowns.projectListDD?.result ?? []}
                    getOptionLabel={(option) => option.name}
                    classNamePrefix={"custom-select-style"}
                    getOptionValue={(option) => option.id.toString()}
                    onChange={(option: $TSFixMe) => {
                      setSelectedFilter((prev) => {
                        return { ...prev, projectId: option.id };
                      });
                      setFieldValue("project", option);
                    }}
                    value={
                      selectedFilter.projectId
                        ? props.dropdowns.projectListDD?.result?.find(
                          (option: any) =>
                            option.id === selectedFilter.projectId
                        )
                        : null
                    }
                    onBlur={handleBlur("project")}
                    maxMenuHeight={180}
                    menuPortalTarget={document.body}
                    styles={{
                      menuPortal: (base: $TSFixMe) => ({
                        ...base,
                        zIndex: 9999,
                      }),
                    }}
                  />
                </Col>
                <Col
                  xl={4}
                  md={4}
                  sm={4}
                  className="mapsDropRight mt-sm-0 mt-2 ml-sm-0 ml-2"
                >
                  <Button
                    // @ts-expect-error TS(2769): No overload matches this call.
                    onClick={handleSubmit}
                    disabled={isSubmitting}
                    className={`pagebtn ${props.theme}`}
                  >
                    {isLoading ?
                      <Spinner color="light"
                        size="sm" className="mr-2" />
                      : "Filter"}
                  </Button>
                </Col>
              </div>
            </>
          )}
        </Formik>
      </>
    </Row>
  );
}

const mapStateToProps = (state: RootState) => ({
  dropdowns: state.dropdownList.dropdowns,
  organizationReports: state.dataStore.organizationReports,
  orgListDD: state.dropdownList.dropdowns.organizationsDD,
  isLoading: state.generalSlice.isLoading,
  theme: state.theme.currentTheme,
  selectedOrgData: state.generalSlice.selectedOrgData,
});

const mapDispatchToProps = (dispatch: AppDispatch) => ({
  startLoading: () => dispatch(startLoading()),
  endLoading: () => dispatch(endLoading()),
  getOrgListDD: () => dispatch(getOrganizationDD()),
  resetStates: () => dispatch(resetStatesSuccess()),
  getProjectListDD: (id: number) => dispatch(getProjectListDD(id)),
  getMapMonitoring: (data: $TSFixMe) => dispatch(getMapMonitoring(data)),
  resetMapData: () => dispatch(resetMapData()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Monitoring);
