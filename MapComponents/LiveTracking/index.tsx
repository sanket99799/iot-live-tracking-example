import { useEffect, useRef, useState } from "react";
import { Row, Col, Button, Spinner } from "reactstrap";
import {
  getDeviceListDD,
  getOrganizationDD,
  getProjectListDD,
  resetStatesSuccess,
} from "../../../../redux/reducers/dropdowns/dropdown.actions";
import {
  endLoading,
  startLoading,
} from "../../../../redux/reducers/general/general.actions";
import { connect } from "react-redux";
import Select from "react-select";
import { Formik } from "formik";
import { liveTrackingValidationSchema } from "../../../../constants";
import { AppDispatch } from "../../../../redux/store/store";
import { RootState } from "../../../../redux/reducers/rootReducer";
import { getLiveTracking } from "../../../../redux/reducers/MapReducer/map.actions";
import { toast } from "react-toastify";

type Props = ReturnType<typeof mapStateToProps> &
  ReturnType<typeof mapDispatchToProps>;

function LiveTracking(props: Props) {
  const [selectedFilter, setSelectedFilter] = useState({
    orgId: null,
    projectId: null,
    vehicleRegNo: null,
  });
  const [isLoading, setIsLoading] = useState(false);
  const [isSubmittingFilter, setIsSubmittingFilter] = useState(false);

  const formikRef: $TSFixMe = useRef();

  const handleFilter = async (values: $TSFixMe, type?: $TSFixMe) => {
    try {
      if (type !== "fetchData") {
        setIsLoading(true);
      }
      let data = {
        organizationManagementId: values.organization,
        projectId: values.project,
        deviceId: values.vehicleReg,
      };
      await props.getLiveTracking(data);
      setTimeout(() => {
        setIsSubmittingFilter(!isSubmittingFilter);
      }, 2000);
    } catch (error) {
      console.log(error);
      toast.error(`${(error as $TSFixMe).data.message}`);
    } finally {
      setIsLoading(false);
    }
  };

  const apiCall = async () => {
    props.startLoading();
    if (props.orgListDD?.result?.length === 0) {
      props.getOrgListDD();
    }
    props.endLoading();
  };

  useEffect(() => {
    if (selectedFilter.orgId) {
      props.getProjectListDD(selectedFilter.orgId);
    }
    return () => { };
  }, [selectedFilter.orgId]);

  useEffect(() => {
    if (selectedFilter.projectId) {
      props.getDeviceListDD(selectedFilter.projectId);
    }
    return () => { };
  }, [selectedFilter.projectId]);

  useEffect(() => {
    const values: $TSFixMe = {
      organization: selectedFilter.orgId,
      project: selectedFilter.projectId,
      vehicleReg: selectedFilter.vehicleRegNo,
    };

    let success = false; // Flag to track success of API call

    const fetchData = async () => {
      if (selectedFilter.orgId && selectedFilter.projectId && selectedFilter.vehicleRegNo) {
        try {
          await handleFilter(values, "fetchData");
          success = true; // API call succeeded
        } catch (error) {
          success = false; // API call failed
        }
      }
    };

    // Initial call when the submit button is clicked
    fetchData();

    // Set up the interval to call the API every 5 seconds
    // const interval = setInterval(fetchData, 5000);
    const interval = setInterval(() => {
      if (success) { // Only call API if the previous call was successful
        fetchData();
      } else {
        clearInterval(interval); // Stop interval if previous call failed
      }
    }, 5000);

    // Clean up the interval when the component is unmounted or when the submit button is not pressed
    return () => clearInterval(interval);
  }, [isSubmittingFilter]);

  useEffect(() => {
    apiCall();
  }, []);

  useEffect(() => {
    if (props.selectedOrgData) {
      setSelectedFilter((prev: $TSFixMe) => {
        return { ...prev, orgId: props.selectedOrgData.id, projectId: null, vehicleRegNo: null }
      });
      formikRef?.current?.setFieldValue("project", "");
      formikRef?.current?.setFieldValue("vehicleReg", "");
      props.resetStates()
    }
    return () => { }
  }, [props.selectedOrgData.id]);


  return (
    <Row>
      <>
        <Formik
          validationSchema={liveTrackingValidationSchema}
          innerRef={formikRef}
          enableReinitialize={true}
          initialValues={{
            vehicleReg: "",
            organization: props.selectedOrgData ? props.selectedOrgData.id : "",
            project: "",
          }}
          onSubmit={handleFilter}
        >
          {({ handleBlur, setFieldValue, setTouched, handleSubmit,
            isSubmitting,
            touched,
            errors, }) => {
            return (
              <>
                <div
                  className={`modulcar mapCard driverScoreCardBody align-items-baseline ${props.theme}`}
                >
                  <Col xl={3} md={3} sm={6} className="mapsDropLeft">
                    <Select
                      className={(errors.organization && touched.organization ? "is-invalid " : "")}
                      placeholder="Organization"
                      options={props.orgListDD?.result || []}
                      getOptionLabel={(option: $TSFixMe) => option.name}
                      getOptionValue={(option: $TSFixMe) => option.id.toString()}
                      classNamePrefix={"custom-select-style"}
                      value={
                        selectedFilter.orgId
                          ? props.orgListDD?.result?.find(
                            (option: any) => option.id === selectedFilter.orgId
                          )
                          : null
                      }
                      onChange={(option: $TSFixMe) => {
                        setTouched({ ...touched, organization: false });
                        setSelectedFilter((prev) => {
                          return {
                            ...prev,
                            orgId: option.id,
                            projectId: null,
                            vehicleRegNo: null,
                          };
                        });
                        setFieldValue("organization", option.id);
                        setFieldValue("project", {});
                        props.resetStates();
                        setFieldValue("vehicleReg", {});
                      }}
                      onBlur={handleBlur("organization")}
                      maxMenuHeight={180}
                      menuPortalTarget={document.body}
                      styles={{
                        menuPortal: (base: $TSFixMe) => ({
                          ...base,
                          zIndex: 9999,
                        }),
                      }}
                    />
                    {touched.organization && errors.organization && (
                      <div className="validate">
                        {errors.organization ? "Required" : errors.organization}
                      </div>
                    )}
                  </Col>
                  <Col xl={3} md={3} sm={6} className="mapsDrop mt-sm-0 mt-2">
                    <Select
                      className={(errors.project && touched.project ? "is-invalid " : "")}
                      placeholder="Project"
                      options={props.dropdowns.projectListDD?.result ?? []}
                      classNamePrefix={"custom-select-style"}
                      getOptionLabel={(option: $TSFixMe) => option.name}
                      getOptionValue={(option: $TSFixMe) => option.id.toString()}
                      onChange={(option: $TSFixMe) => {
                        setTouched({ ...touched, project: false });
                        setSelectedFilter((prev) => {
                          return {
                            ...prev,
                            projectId: option.id,
                            vehicleRegNo: null,
                          };
                        });
                        setFieldValue("project", option.id);
                        setFieldValue("vehicleReg", {});
                      }}
                      value={
                        selectedFilter.projectId
                          ? props.dropdowns.projectListDD?.result?.find(
                            (option: any) =>
                              option.id === selectedFilter.projectId
                          )
                          : null
                      }
                      onBlur={handleBlur("project")}
                      maxMenuHeight={180}
                      menuPortalTarget={document.body}
                      styles={{
                        menuPortal: (base: $TSFixMe) => ({
                          ...base,
                          zIndex: 9999,
                        }),
                      }}
                    />
                    {touched.project && errors.project && (
                      <div className="validate">
                        {errors.project ? "Required" : errors.project}
                      </div>
                    )}
                  </Col>
                  <Col
                    xl={3}
                    md={3}
                    sm={6}
                    className="mapsDropRight mt-md-0 mt-2"
                  >
                    <Select
                      className={(errors.vehicleReg && touched.vehicleReg ? "is-invalid " : "")}
                      placeholder="Vehicle Reg#"
                      options={props?.dropdowns?.deviceListDD?.result || []}
                      classNamePrefix={"custom-select-style"}
                      getOptionLabel={(option: $TSFixMe) => option.name ?? option.otherValue1}
                      getOptionValue={(option: $TSFixMe) => option.id.toString()}
                      onChange={(option: $TSFixMe) => {
                        setSelectedFilter((prev: $TSFixMe) => {
                          return { ...prev, vehicleRegNo: option.otherValue1 };
                        });
                        setFieldValue("vehicleReg", option.otherValue1);
                      }}
                      value={
                        selectedFilter.vehicleRegNo
                          ? props?.dropdowns?.deviceListDD?.result?.find(
                            (option: any) =>
                              option.otherValue1 == selectedFilter.vehicleRegNo
                          )
                          : null
                      }
                      onBlur={handleBlur("vehicleReg")}
                      maxMenuHeight={180}
                      menuPortalTarget={document.body}
                      styles={{
                        menuPortal: (base: $TSFixMe) => ({
                          ...base,
                          zIndex: 9999,
                        }),
                      }}
                    />
                    {touched.vehicleReg && errors.vehicleReg && (
                      <div className="validate">
                        {errors.vehicleReg ? "Required" : errors.vehicleReg}
                      </div>
                    )}
                  </Col>
                  <Col
                    xl={3}
                    md={3}
                    sm={6}
                    className="mapsDropRight mt-md-0 mt-2"
                  >
                    <Button
                      // @ts-expect-error TS(2769): No overload matches this call.
                      onClick={handleSubmit}
                      disabled={isLoading}
                      className={`pagebtn ${props.theme}`}
                    >
                      {isLoading ?
                        <Spinner color="light"
                          size="sm" className="mr-2" />
                        : "Filter"}

                    </Button>
                  </Col>
                </div>
              </>
            )
          }}
        </Formik>
      </>
    </Row>
  );
}

const mapStateToProps = (state: RootState) => ({
  dropdowns: state.dropdownList.dropdowns,
  deviceReports: state.dataStore.deviceReports,
  orgListDD: state.dropdownList.dropdowns.organizationsDD,
  isLoading: state.generalSlice.isLoading,
  theme: state.theme.currentTheme,
  selectedOrgData: state.generalSlice.selectedOrgData,
});

const mapDispatchToProps = (dispatch: AppDispatch) => ({
  startLoading: () => dispatch(startLoading()),
  endLoading: () => dispatch(endLoading()),
  getOrgListDD: () => dispatch(getOrganizationDD()),
  resetStates: () => dispatch(resetStatesSuccess()),
  getDeviceListDD: (id: number) => dispatch(getDeviceListDD(id)),
  getProjectListDD: (id: number) => dispatch(getProjectListDD(id)),
  getLiveTracking: (data: $TSFixMe) => dispatch(getLiveTracking(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(LiveTracking);
