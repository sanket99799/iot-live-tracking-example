import { Map, TileLayer, Marker, Popup, LayersControl } from "react-leaflet";
import { useEffect, useState } from "react";
import {
  convertTimestampToDate,
  getCustomIcon,
} from "../../../../../services/common.functions.services";
import {
  MAP_DEFAULT_LAT_LONG,
  MAP_SCROLLWHEELZOOM,
  MAP_ZOOM,
} from "../../../../../constants";

const { BaseLayer } = LayersControl;

function LiveTrackingLoc(props: $TSFixMe) {
  const [markersData, setMarkersData]: $TSFixMe = useState({});
  const [mapZoom, setMapZoom] = useState(MAP_ZOOM);

  const carAngle = markersData.heading || 0;
  const customIcon = getCustomIcon(carAngle);

  const handleGetMapData = async () => {
    if (!props.markerData?.latitude) {
      setMarkersData({});
      return;
    }
    if (Object.keys(props.markerData).length > 0) {
      const marker: $TSFixMe = {
        position: [props.markerData.latitude, props.markerData.longitude],
        ...props.markerData,
      };
      setMarkersData(marker);
    } else {
      setMarkersData({});
    }
  };

  useEffect(() => {
    if (props.markerData) {
      handleGetMapData();
    }
    return () => { };
  }, [props.markerData]);

  return (
    <Map
      className="mapLoc"
      center={
        Object.keys(markersData).length > 0 && markersData.position[0]
          ? markersData?.position
          : MAP_DEFAULT_LAT_LONG
      }
      zoom={mapZoom}
      onzoom={(e:$TSFixMe) => setMapZoom(e.target._zoom)}
      scrollWheelZoom={MAP_SCROLLWHEELZOOM}
    >
      <LayersControl position="topright">
        <BaseLayer checked name="Base Map">
          <TileLayer url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" />
        </BaseLayer>
        <BaseLayer name="Humanitarian">
          <TileLayer url="https://cartodb-basemaps-{s}.global.ssl.fastly.net/dark_all/{z}/{x}/{y}.png" />
        </BaseLayer>
        <BaseLayer name="Satellite">
          <TileLayer url="https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}" />
        </BaseLayer>

        {Object.keys(markersData).length > 0 && (
          <Marker
            position={markersData.position[0] ? markersData.position : []}
            icon={customIcon}
          >
            <Popup>
              <div>
                <b>Device Id:</b> {markersData?.deviceId ?? ""}
              </div>
              <div>
                <b>Date & Time:</b>{" "}
                {markersData?.timestamp
                  ? convertTimestampToDate(
                    props.user.timeZone.zoneId,
                    markersData?.timestamp,
                    null,
                    props.user.timeFormat
                  )
                  : "-"}
              </div>
            </Popup>
          </Marker>
        )}
      </LayersControl>
    </Map>
  );
}

export default LiveTrackingLoc;
