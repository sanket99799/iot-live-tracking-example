import { useEffect, useRef, useState } from "react";
import { Row, Col, Input, Button, Spinner } from "reactstrap";
import { DatePicker } from "antd";
import "antd/dist/antd.css";
import { connect } from "react-redux";
import Select from "react-select";
import { Field, Formik } from "formik";
import { getDeviceListDD, getOrganizationDD, getProjectListDD, resetStatesSuccess } from "../../../../redux/reducers/dropdowns/dropdown.actions";
import {
  endLoading,
  startLoading,
} from "../../../../redux/reducers/general/general.actions";
import { vehicleJourneyValidationSchema } from "../../../../constants";
import { RootState } from "../../../../redux/reducers/rootReducer";
import { AppDispatch } from "../../../../redux/store/store";
import { getVehicleJourney, resetMapData } from "../../../../redux/reducers/MapReducer/map.actions";
import { toast } from "react-toastify";
import { convertDateToTimestamp } from "../../../../services/common.functions.services";

const { RangePicker } = DatePicker;

type Props = ReturnType<typeof mapStateToProps> &
  ReturnType<typeof mapDispatchToProps>;

function VehicleJourney(props: Props) {
  const [selectedFilter, setSelectedFilter] = useState({
    orgId: null,
    projectId: null,
    vehicleRegNo: null,
    date: null,
  });
  const formikRef: $TSFixMe = useRef();

  const [isLoading, setIsLoading] = useState(false);

  const apiCall = async () => {
    props.startLoading();
    if (props.orgListDD?.result?.length === 0) {
      props.getOrgListDD();
    }
    props.endLoading();
  };

  const handleFilter = async (values: $TSFixMe) => {
    try {
      setIsLoading(true);
      let data = {
        organizationManagementId: values.organization,
        projectId: values?.project,
        deviceId: values?.vehicleReg,
        startTimestamp: values.date[0] ? convertDateToTimestamp(values?.date[0], props?.user?.timeZone?.zoneId) : null,
        endTimestamp: values.date[1] ? convertDateToTimestamp(values?.date[1], props?.user?.timeZone?.zoneId) : null,
      };
      await props.getVehicleJourney(data);
    } catch (error) {
      console.log(error);
      toast.error(`${(error as $TSFixMe).data.message}`);
    } finally {
      setIsLoading(false);

    }
  };

  useEffect(() => {
    if (selectedFilter.orgId) {
      props.getProjectListDD(selectedFilter.orgId);
    }
    return () => { }
  }, [selectedFilter.orgId]);

  useEffect(() => {
    if (selectedFilter.projectId) {
      props.getDeviceListDD(selectedFilter.projectId);
    }
    return () => { }
  }, [selectedFilter.projectId]);

  useEffect(() => {
    if (!selectedFilter.orgId && !selectedFilter.projectId) {
      props.resetMapData();
    }
    return () => {
      props.resetMapData();
    };
  }, [selectedFilter]);

  useEffect(() => {
    apiCall();
  }, []);

  useEffect(() => {
    if (props.selectedOrgData) {
      setSelectedFilter((prev: $TSFixMe) => {
        return { ...prev, orgId: props.selectedOrgData.id, projectId: null, vehicleRegNo: null }
      });
      formikRef?.current?.setFieldValue("project", "");
      formikRef?.current?.setFieldValue("vehicleReg", "");
      props.resetStates()
    }
    return () => { }
  }, [props.selectedOrgData.id]);

  return (
    <Row>
      <Formik
        validationSchema={vehicleJourneyValidationSchema}
        enableReinitialize={true}
        innerRef={formikRef}
        initialValues={{
          vehicleReg: "",
          organization: props.selectedOrgData ? props.selectedOrgData.id : "",
          project: "",
          date: "",
        }}
        onSubmit={handleFilter}
      >
        {({ handleBlur, setFieldValue, handleSubmit,
          isSubmitting,
          touched,
          setTouched,
          values,
          errors, }) => {
          return (
            <>
              <div
                className={`modulcar mapCard driverScoreCardBody ${props.theme}`}
                style={{ alignItems: "flex-start" }}
              >
                <div className="w-18 px-1">
                  <Select
                    className={(errors.organization && touched.organization ? "is-invalid " : "")}
                    placeholder="Organization"
                    options={props.orgListDD?.result || []}
                    getOptionLabel={(option: $TSFixMe) => option.name}
                    classNamePrefix={"custom-select-style"}
                    getOptionValue={(option: $TSFixMe) => option.id.toString()}
                    onChange={(option: $TSFixMe) => {
                      setTouched({ ...touched, organization: false });
                      setSelectedFilter((prev) => {
                        return { ...prev, orgId: option.id, projectId: null, vehicleRegNo: null }
                      });
                      setFieldValue("organization", option.id);
                      setFieldValue("project", {});
                      props.resetStates();
                      setFieldValue("vehicleReg", {});
                    }}
                    value={
                      selectedFilter.orgId
                        ? props.orgListDD?.result?.find(
                          (option: any) => option.id === selectedFilter.orgId
                        )
                        : null
                    }
                    onBlur={handleBlur("organization")}
                    maxMenuHeight={180}
                    menuPortalTarget={document.body}
                    styles={{
                      menuPortal: (base: $TSFixMe) => ({
                        ...base,
                        zIndex: 9999,
                      }),
                    }}
                  />
                  {touched.organization && errors.organization && (
                    <div className="validate">
                      {errors.organization ? "Required" : errors.organization}
                    </div>
                  )}
                </div>
                <div className="w-18 px-1">
                  <Select
                    placeholder="Project"
                    className={(errors.project && touched.project ? "is-invalid " : "")}
                    options={props.dropdowns.projectListDD?.result ?? []}
                    classNamePrefix={"custom-select-style"}
                    getOptionLabel={(option: $TSFixMe) => option.name}
                    getOptionValue={(option: $TSFixMe) => option.id.toString()}
                    onChange={(option: $TSFixMe) => {
                      setTouched({ ...touched, project: false });
                      setSelectedFilter((prev) => {
                        return { ...prev, projectId: option.id, vehicleRegNo: null }
                      });
                      setFieldValue("project", option.id);
                      setFieldValue("vehicleReg", {});
                    }}
                    value={
                      selectedFilter.projectId
                        ? props.dropdowns.projectListDD?.result?.find(
                          (option: any) => option.id === selectedFilter.projectId
                        )
                        : null
                    }
                    onBlur={handleBlur("project")}
                    maxMenuHeight={180}
                    menuPortalTarget={document.body}
                    styles={{
                      menuPortal: (base: $TSFixMe) => ({
                        ...base,
                        zIndex: 9999,
                      }),
                    }}
                  />
                  {touched.project && errors.project && (
                    <div className="validate">
                      {errors.project ? "Required" : errors.project}
                    </div>
                  )}
                </div>
                <div className="w-20 px-1">
                  <Select
                    className={(errors.vehicleReg && touched.vehicleReg ? "is-invalid " : "")}
                    placeholder="Vehicle Reg#"
                    options={props?.dropdowns?.deviceListDD?.result || []}
                    classNamePrefix={"custom-select-style"}
                    getOptionLabel={(option: $TSFixMe) => option.name ?? option.otherValue1}
                    getOptionValue={(option: $TSFixMe) => option.id.toString()}
                    onChange={(option: $TSFixMe) => {
                      setSelectedFilter((prev) => {
                        return { ...prev, vehicleRegNo: option.otherValue1 }
                      });
                      setFieldValue("vehicleReg", option.otherValue1);
                    }}
                    value={
                      selectedFilter.vehicleRegNo
                        ? props?.dropdowns?.deviceListDD?.result?.find(
                          (option: any) => option.otherValue1 === selectedFilter.vehicleRegNo
                        )
                        : null
                    }
                    onBlur={handleBlur("vehicleReg")}
                    maxMenuHeight={180}
                    menuPortalTarget={document.body}
                    styles={{
                      menuPortal: (base: $TSFixMe) => ({
                        ...base,
                        zIndex: 9999,
                      }),
                    }}
                  />
                  {touched.vehicleReg && errors.vehicleReg && (
                    <div className="validate">
                      {errors.vehicleReg ? "Required" : errors.vehicleReg}
                    </div>
                  )}
                </div>
                <div className="w-36 px-1">
                  <Field
                    component={RangePicker}
                    name="date"
                    className={`w-100 rangeHei ${(errors.date && touched.date ? "is-invalid " : "")}`}
                    onChange={(date: $TSFixMe, dateString: $TSFixMe) => {
                      if (dateString[0] !== "") {
                        setSelectedFilter((prev) => {
                          return { ...prev, date: dateString }
                        });
                        setFieldValue("date", dateString);
                      } else {
                        setSelectedFilter((prev) => {
                          return { ...prev, date: null }
                        });
                        setFieldValue("date", null);
                      }
                    }}
                    onBlur={handleBlur("date")}
                    format="MM-DD-YYYY HH:mm:ss"
                    showTime
                    inputProps={{
                      onBlur: handleBlur("date"),
                    }}
                    menuPortalTarget={document.body}
                    styles={{
                      menuPortal: (base: $TSFixMe) => ({
                        ...base,
                        zIndex: 9999,
                      }),
                    }}
                  />
                  {touched.date && errors.date && (
                    <div className="validate">
                      {errors.date ? "Required" : errors.date}
                    </div>
                  )}
                </div>
                {/* <Col xl={4} md={4} className="mapsDrop mt-2"> */}
                {/* <Input
                    className=""
                    autoComplete="disable"
                    placeholder="2"
                    style={{ width: "100%" }}
                  />
                  <span className="mapspan">
                    * Accuracy is in seconds and it is interval between two GPS
                    location.
                  </span> */}
                {/* </Col> */}
                <div className="w-8 px-1 mtb-2">
                  <Button
                    className={`pagebtn ${props.theme}`}
                    // @ts-expect-error TS(2769): No overload matches this call.
                    onClick={handleSubmit}
                    disabled={isSubmitting}
                  >
                    {isLoading ?
                      <Spinner color="light"
                        size="sm" />
                      : "Filter"}
                  </Button>
                </div>
              </div>
            </>
          )
        }}
      </Formik>
    </Row>
  );
}

//export default VehicleJourney;
const mapStateToProps = (state: RootState) => ({
  dropdowns: state.dropdownList.dropdowns,
  eventReports: state.dataStore.eventReports,
  orgListDD: state.dropdownList.dropdowns.organizationsDD,
  isLoading: state.generalSlice.isLoading,
  theme: state.theme.currentTheme,
  user: state.userSlice.user,
  selectedOrgData: state.generalSlice.selectedOrgData,
});

const mapDispatchToProps = (dispatch: AppDispatch) => ({
  startLoading: () => dispatch(startLoading()),
  endLoading: () => dispatch(endLoading()),
  getOrgListDD: () => dispatch(getOrganizationDD()),
  getDeviceListDD: (id: number) => dispatch(getDeviceListDD(id)),
  resetStates: () => dispatch(resetStatesSuccess()),
  getProjectListDD: (id: number) => dispatch(getProjectListDD(id)),
  getVehicleJourney: (data: $TSFixMe) => dispatch(getVehicleJourney(data)),
  resetMapData: () => dispatch(resetMapData()),
});

export default connect(mapStateToProps, mapDispatchToProps)(VehicleJourney);
