import { useEffect, useState } from "react";
import { Card, Row, Col, CardBody } from "reactstrap";
import Monitoring from "../Monitoring";
import LiveTracking from "../LiveTracking";
import VehicleJourney from "../VehicleJourney";
import LiveTrackingLoc from "../LiveTracking/Loc";
import { RootState } from "../../../../redux/reducers/rootReducer";
import { connect } from "react-redux";
import MonitoringLoc from "../Monitoring/Loc";
import VehicleJourneyMap from "../../../Common/Map/VehicleJourneyMap";
import usePermission from "../../../../constants/usePermission";

type Props = ReturnType<typeof mapStateToProps>;

function MapCard(props: Props) {
  const { theme: selectedTheme } = props;
  const [Selected, setSelected] = useState("Monitoring");
  const monitoringReadWrite = usePermission(["MONITORING_READ", "MONITORING_WRITE"]);
  const trackingReadWrite = usePermission(["LIVE_TRACKING_READ", "LIVE_TRACKING_WRITE"]);
  const vehicleReadWrite = usePermission(["VEHICLE_JOURNEY_READ", "VEHICLE_JOURNEY_WRITE"]);

  useEffect(() => {
    if (monitoringReadWrite) {
      setSelected("Monitoring");
    } else if (trackingReadWrite) {
      setSelected("LiveTracking");
    } else if (vehicleReadWrite) {
      setSelected("VehicleJourney");
    }
    return () => { }
  }, [monitoringReadWrite, trackingReadWrite, vehicleReadWrite]);

  return (
    <>
      <Row className="mt-0 d-flex flex-wrap">
        <Col className="tabAll">
          <Card
            className={`card-stats md-4 mb-xl-0 devi allPage shadowCard ${selectedTheme}`}
          >
            <div className="modulcar">
              <Row className="pb-1">
                <Col>
                  <div
                    className={`mt-2 mapstabs d-flex flex-wrap justify-content-center ${selectedTheme}`}
                  >
                    {monitoringReadWrite &&
                      <button
                        className={
                          Selected === "Monitoring"
                            ? "maptoggle active"
                            : "maptoggle"
                        }
                        onClick={() => setSelected("Monitoring")}
                      >
                        Monitoring
                      </button>
                    }
                    {trackingReadWrite &&
                      <button
                        className={
                          Selected === "LiveTracking"
                            ? "maptoggle  active"
                            : "maptoggle "
                        }
                        onClick={() => setSelected("LiveTracking")}
                      >
                        Live Tracking
                      </button>
                    }
                    {vehicleReadWrite &&
                      <button
                        className={
                          Selected === "VehicleJourney"
                            ? "maptoggle active"
                            : "maptoggle"
                        }
                        onClick={() => setSelected("VehicleJourney")}
                      >
                        Vehicle Journey
                      </button>
                    }
                  </div>
                </Col>
              </Row>
              <br />
              {Selected === "Monitoring" && <Monitoring />}
              {Selected === "LiveTracking" && <LiveTracking />}
              {Selected === "VehicleJourney" && <VehicleJourney />}
              <br />
            </div>
          </Card>
        </Col>
      </Row>
      <Row className="mt-3">
        <Col className="tabAll">
          <>
            <Card
              className={`card-stats md-4 mb-xl-0 devi card-fixed ${props.theme} shadowCard`}
            >
              <CardBody className="allPageTab">
                {Selected === "LiveTracking" && <LiveTrackingLoc markerData={props?.map?.liveTracking} user={props.user} />}
                {Selected === "Monitoring" && <MonitoringLoc markerData={props?.map?.monitoring} user={props.user} />}
                {Selected === "VehicleJourney" && <VehicleJourneyMap markerData={props?.map?.vehicleJourney} user={props.user} />}
              </CardBody>
            </Card>
          </>
        </Col>
      </Row>
    </>
  );
}

const mapStateToProps = (state: RootState) => ({
  theme: state.theme.currentTheme,
  user: state.userSlice.user,
  map: state.map,
});

export default connect(mapStateToProps)(MapCard);
