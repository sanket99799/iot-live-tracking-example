import Paper from "@material-ui/core/Paper";
import { Card, CardBody, Container, Row, Col } from "reactstrap";
import Loc from "./LiveTracking/Loc";
import MapCard from "./MapCard";
import CopyrightFooter from "../../Common/CopyrightFooter";
import FullPageLoaderModal from "../../Common/FullPageLoader/FullPageLoaderModal";
import { connect } from "react-redux";
import { RootState } from "../../../redux/reducers/rootReducer";

type Props = ReturnType<typeof mapStateToProps> 
function Map(props: Props) {
  return (
    <div className="mainPageheader bg-gradient-info pb-1 pt-3 pt-md-8">
      <FullPageLoaderModal isLoading={props.isLoading} />
      <Container className="mt--7" fluid>
        <MapCard />
      </Container>
    </div>
  );
}

const mapStateToProps = (state: RootState) => ({
  isLoading: state.generalSlice.isLoading,
});

export default connect(mapStateToProps)(Map);
